#!/bin/env python


import argparse
import configparser
import json
import os
import sys

sys.path.append('..')
sys.path.append(os.getcwd())

from tweenote.annotation import Spotlight
from tweenote.extraction import PairExtractor
from tweenote.extraction import HashtagExtractor
from tweenote.typing import TRank


# noinspection PyShadowingNames
def configuration():
    config = configparser.RawConfigParser()
    filename = None
    paths = ['config.ini', '../config.ini']
    for path in paths:
        if os.path.isfile(path):
            filename = path
            break
    if filename:
        config.read(filename)
    return config


if __name__ == '__main__':
    config = configuration()

    parser = argparse.ArgumentParser()
    parser.add_argument("-k", "--key", help="The identifier of the text")
    parser.add_argument("-t", "--text", help="The text to be annotated")
    args = parser.parse_args()

    tagme_endpoint = config['tagme']['uri']
    tagme_gcube_token = config['tagme']['gcube-token']

    spotlight_host = config['dbpedia-spotlight']['host']
    spotlight_port = int(config['dbpedia-spotlight']['port'])
    spotlight_confidence = float(config['dbpedia-spotlight']['confidence'])
    spotlight_support = float(config['dbpedia-spotlight']['support'])
    trank_endpoint = config['trankapi']['uri']

    annotator = Spotlight(host=spotlight_host, port=spotlight_port, confidence=spotlight_confidence,
                          support=spotlight_support)
    entyper = TRank(endpoint=trank_endpoint)
    extractor = PairExtractor(annotator=annotator, entyper=entyper)


    args.text='\'It\'s been a massive, massive rally\': Why investors have welcomed a Trump presidency #Trump http://www.cbc.ca/1.3896750'
    args.key='8096089992304721921'
    extractor = HashtagExtractor()
    objects = extractor.objects({'text':args.text, 'id_str':args.key})
    print(json.dumps(objects, default=lambda o: o.__dict__, indent=4, separators=(',', ': ')))
