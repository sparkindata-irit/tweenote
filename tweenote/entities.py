import json

from tweenote.cache import Resource
from tweenote.lod import Dbpedia
from tweenote.lod import LOD
from tweenote.text import TextUtils


class NamedEntity(Resource):
    default_path = 'cache'
    default_resource = 'entities'

    def __init__(self, id=None, path=None, resource=None):
        super().__init__(path=NamedEntity.default_path if not path  else path,
                         resource=NamedEntity.default_resource if not resource else resource)
        self.id = id
        self.annotator = None
        self.mention = None
        self.score = None
        self.name = None
        self.uri = None

    def __str__(self):
        return json.dumps(self, default=lambda o: o.__dict__)

    def label(self, refresh=False):
        self.resource = 'entities/label'
        key = TextUtils.normalize(self.uri)
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not data or refresh:
            try:
                if self.uri:
                    label = self.uri
                    if self.uri.startswith('http://dbpedia.org/class/yago/'):
                        label = self.uri.replace('http://dbpedia.org/class/yago/', '')
                        return TextUtils.uncamel(label)
                    elif self.uri.startswith('http://schema.org/'):
                        return self.uri.replace('http://schema.org/', '')
                    elif self.uri.startswith('http://dbpedia.org/'):
                        dbpedia = Dbpedia()
                        label = dbpedia.label(self.uri)
                    elif self.uri == label:
                        sparql = LOD(endpoint='http://live.dbpedia.org/')
                        label = sparql.label(self.uri)
                    data = label
                if key:
                    self.write(data, key, options={'json': True})
            except:
                pass
        return data
