import configparser
import os
import re

from nltk.corpus import stopwords
from nltk.util import ngrams

from tweenote.annotation import Spotlight
from tweenote.annotation import TwitterNLP
from tweenote.cache import Resource
from tweenote.twitter import TwitterSearch
from tweenote.text import TextUtils


class Keyphrases(Resource):
    default_path = 'cache'
    default_resource = 'keyphrases'

    data = {'keyphrases': {
        '1gram': [],
        '2grams': [],
        '3grams': [],
        'ngrams': [],
        'pos_nouns': [],
        'pos_verbs': [],
        'chunks': [],
        'entities': [],
    },
        'urls': []
    }

    def __init__(self, path=default_path, resource=default_resource):
        super().__init__(path, resource)

    def init(self):
        self.data = {'keyphrases': {
            '1gram': [],
            '2grams': [],
            '3grams': [],
            'ngrams': [],
            'pos_nouns': [],
            'pos_verbs': [],
            'chunks': [],
            'entities': [],
        },
            'urls': []
        }

    def addToValues(value, values, repeated=False, attributes={}):
        norm = TextUtils.normalize(value)
        if norm not in values and norm not in stopwords.words('english') and not re.match("[\W\s]+", norm):
            values[norm] = {'word': value, 'cf': 0, 'df': 0, 'attributes': attributes}
        values[norm]['cf'] += 1
        if (not repeated):
            values[norm]['df'] += 1

    def getChunks(pos):
        cchunks = []
        for word in pos['words']:
            if word['chunk'] != None and not word['word'].startswith("http"):
                if (word['chunk'].endswith('-NP')):
                    start = word['chunk'] == None
                    if (not start):
                        start = not word['chunk'].startswith('I-')
                    if (start):
                        cchunks.append(word['word'])
                    else:
                        cchunks[len(cchunks) - 1] += " " + word['word']
        for word in pos['words']:
            if word['chunk'] != None and not word['word'].startswith("http"):
                if (word['chunk'].endswith('-VP')):
                    start = word['chunk'] == None
                    if (not start):
                        start = not word['chunk'].startswith('I-')
                    if (start):
                        cchunks.append(word['word'])
                    else:
                        cchunks[len(cchunks) - 1] += " " + word['word']

        ccchunks = []
        for c in cchunks:
            if c.lower() not in stopwords.words('english') and re.match("\W+", c):
                ccchunks.append(c)
        return ccchunks

    def add_url(self, url, repeated=False):
        found = False
        for tmp in self.data['urls']:
            if tmp['url'] == url:
                tmp['cf'] += 1
                if not repeated:
                    tmp['df'] += 1
                found = True
                break
        if not found:
            self.data['urls'].append({'url': url, 'cf': 1, 'df': 1})

    def search(self, query):
        config = configparser.RawConfigParser()
        config.read(os.getcwd() + '\config.ini')

        twitter_options = {}
        if 'twitter' in config:
            twitter_options = {'consumer_key': config['twitter']['consumer_key'],
                               'consumer_secret': config['twitter']['consumer_secret'],
                               'access_token': config['twitter']['access_token'],
                               'access_token_secret': config['twitter']['access_token_secret']}

        twitter = TwitterSearch(options=twitter_options)

        key = TextUtils.normalize(query)

        self.path = 'cache'
        self.resource = 'tweenote/search'
        data_search = None
        if key:
            data = self.read(key, options={'json': True})
        if not os.path.exists(self.file_name(key, options={'json': True})) and not data:
            data = twitter.search(query)
            if key:
                self.write(data, key, options={'json': True})
            data_search = data

        self.resource = 'tweenote/extract'
        data_extract = None
        if key:
            data = self.read(key, options={'json': True})
        if not os.path.exists(self.file_name(key, options={'json': True})) and not data:
            data = self.extract(data_search['statuses'], 2)
            if key:
                self.write(data, key, options={'json': True})
            data_extract = data

        return data

    def preprocess(self, text):
        text = re.sub(r"https?:\/\/.+?\/\w+", ' ', text, flags=re.MULTILINE)
        return text

    def extract(self, statuses, support=0.15):
        self.init()
        nouns = {}
        verbs = {}
        chunks = {}
        annotations = {}
        one_grams = {}
        two_grams = {}
        three_grams = {}
        n_grams = {}
        spotlight = Spotlight()
        twitter_nlp = TwitterNLP()
        analyzed = []
        surfaces = []
        l = 0;
        for status in statuses:

            l += 1
            text = status['text']
            proc_text = self.preprocess(text)
            if text not in analyzed:
                analyzed.append(text)
            urls = []

            if status['entities']:
                if status['entities']['urls']:
                    for url in status['entities']['urls']:
                        repeated = False
                        if url['expanded_url'] in urls:
                            repeated = True
                        self.add_url(url['expanded_url'], repeated)
                        urls.append(url['expanded_url'])

            try:

                spots = spotlight.annotate(text, key=status['id_str'])
                added = []
                for resource in spots.get("Resources"):
                    norm = resource['@URI']
                    value = re.sub(r'[^\w\s]', ' ', resource['@surfaceForm']).strip()
                    Keyphrases.addToValues(value, annotations, repeated=norm in added,
                                           attributes={'uri': resource['@URI']})
                    if (norm not in added):
                        added.append(norm)
            except:
                pass

            pos = twitter_nlp.pos_tagger(text, status['id_str'])

            try:

                added = []
                for word in pos['words']:
                    if not word['word'].startswith("http") and word['word'].lower() not in stopwords.words('english'):
                        if (word['pos'].startswith('NN')):
                            noun = word['word']
                            norm = TextUtils.normalize(noun)
                            value = re.sub(r'[^\w\s]', ' ', word['word']).strip()
                            Keyphrases.addToValues(value, nouns, repeated=norm in added)
                            if (norm not in added):
                                added.append(norm)
            except:
                pass
            try:
                added = []
                for word in pos['words']:
                    if not word['word'].startswith("http") and word['word'].lower() not in stopwords.words('english'):
                        if (word['pos'].startswith('VB')):
                            noun = word['word']
                            norm = TextUtils.normalize(noun)
                            value = re.sub(r'[^\w\s]', ' ', word['word']).strip()
                            Keyphrases.addToValues(value, verbs, repeated=norm in added)
                            if (norm not in added):
                                added.append(norm)
            except:
                pass

            try:
                added = []

                allgrams = []

                gramys = []
                for g in ngrams(proc_text.split(), 1):
                    gramys.append(' '.join(g))
                gramys = set(gramys)
                for gram in gramys:
                    allgrams.append(gram)
                    value = gram
                    value = re.sub(r'[^\w\s]', ' ', value)
                    norm = TextUtils.normalize(value).strip()
                    Keyphrases.addToValues(value, one_grams, repeated=norm in added)
                    if norm not in added:
                        added.append(norm)

                gramys = []
                for g in ngrams(proc_text.split(), 2):
                    gramys.append(' '.join(g))
                gramys = set(gramys)
                for gram in gramys:
                    allgrams.append(gram)
                    value = gram
                    value = re.sub(r'[^\w\s]', ' ', value)
                    norm = TextUtils.normalize(value).strip()
                    Keyphrases.addToValues(value, two_grams, repeated=norm in added)
                    if norm not in added:
                        added.append(norm)

                gramys = []
                for g in ngrams(proc_text.split(), 3):
                    gramys.append(' '.join(g))
                gramys = set(gramys)
                for gram in gramys:
                    allgrams.append(gram)
                    value = gram
                    value = re.sub(r'[^\w\s]', ' ', value)
                    norm = TextUtils.normalize(value).strip()
                    Keyphrases.addToValues(value, three_grams, repeated=norm in added)
                    if norm not in added:
                        added.append(norm)

                gramys = set(allgrams)
                for gram in gramys:
                    value = gram
                    value = re.sub(r'[^\w\s]', ' ', value)
                    norm = TextUtils.normalize(value).strip()
                    Keyphrases.addToValues(value, n_grams, repeated=norm in added)
                    if norm not in added:
                        added.append(norm)

            except:
                pass

            try:
                added = []

                cks = Keyphrases.getChunks(pos)
                for chunk in cks:
                    value = chunk
                    norm = TextUtils.normalize(value)
                    value = re.sub(r'[^\w\s]', ' ', value).strip()
                    Keyphrases.addToValues(value, chunks, repeated=norm in added)
                    if (norm not in added):
                        added.append(norm)

            except:
                pass

        if support < 1:
            support = l * support;

        for key, value in one_grams.items():
            word = value['word']
            cf = value['cf']
            df = value['df']
            if (df >= support):
                self.data['keyphrases']['1gram'].append({'text': word, 'cf': cf, 'df': df})
        for key, value in two_grams.items():
            word = value['word']
            cf = value['cf']
            df = value['df']
            if (df >= support):
                self.data['keyphrases']['2grams'].append({'text': word, 'cf': cf, 'df': df})
        for key, value in three_grams.items():
            word = value['word']
            cf = value['cf']
            df = value['df']
            if (df >= support):
                self.data['keyphrases']['3grams'].append({'text': word, 'cf': cf, 'df': df})
        for key, value in n_grams.items():
            word = value['word']
            cf = value['cf']
            df = value['df']
            if (df >= support):
                self.data['keyphrases']['ngrams'].append({'text': word, 'cf': cf, 'df': df})
        for key, value in nouns.items():
            word = value['word']
            cf = value['cf']
            df = value['df']
            if (df >= support):
                self.data['keyphrases']['pos_nouns'].append({'text': word, 'cf': cf, 'df': df})
        for key, value in verbs.items():
            word = value['word']
            cf = value['cf']
            df = value['df']
            if (df >= support):
                self.data['keyphrases']['pos_verbs'].append({'text': word, 'cf': cf, 'df': df})
        for key, value in chunks.items():
            word = value['word']
            cf = value['cf']
            df = value['df']
            if (df >= support):
                self.data['keyphrases']['chunks'].append({'text': word, 'cf': cf, 'df': df})
        for key, value in annotations.items():
            word = value['word']
            cf = value['cf']
            df = value['df']
            item = {'text': word, 'cf': cf, 'df': df}
            for attribute in value['attributes']:
                item[attribute] = value['attributes'][attribute]
            if df >= support and word not in ['RT', 'rt', 'http', 'https', 'bit.ly']:
                self.data['keyphrases']['entities'].append(item)

        return self.data
