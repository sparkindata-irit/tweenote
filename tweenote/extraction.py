import json
import math
import re
from abc import ABCMeta, abstractmethod

from tweenote.annotation import Spotlight
from tweenote.annotation import TwitterNLP
from tweenote.cache import Resource
from tweenote.entities import NamedEntity
from tweenote.lod import Dbpedia
from tweenote.pair import Pair


class EObject:
    def __init__(self, object):
        self.object = object

    def __str__(self):
        return str(self.object)

    def __repr__(self):
        return json.dumps(self, default=lambda o: o.__dict__)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.object.lower() == other.object.lower()
        else:
            return False

    def json(self):
        return json.dumps(self, default=lambda o: o.__dict__)


class IExtractor(metaclass=ABCMeta):
    @abstractmethod
    def objects(self, status):
        pass


class HashtagExtractor(Resource, IExtractor):
    default_path = 'cache'
    default_resource = 'objects/hashtags'

    def __init__(self, path=None, resource=None):
        super().__init__(path=HashtagExtractor.default_path if not path  else path,
                         resource=HashtagExtractor.default_resource if not resource  else resource)

    def read(self, key=None, options=None):
        objects = []
        data = super().read(key=key, options=options)
        if data:
            for object in data:
                objects.append(EObject(object['object']))
        return objects

    def objects(self, status, refresh=False):
        data = None
        if status['id_str']:
            data = self.read(status['id_str'], options={'json': True})
        if not data or refresh:
            try:
                data = []
                if 'entities' in status:
                    if 'hashtags' in status['entities']:
                        for hashtag in status['entities']['hashtags']:
                            data.append(EObject(hashtag['text']))
                if status['id_str']:
                    self.write(data, status['id_str'], options={'json': True})
            except:
                pass
        return data


class MediaExtractor(Resource, IExtractor):
    default_path = 'cache'
    default_resource = 'objects/media'

    def __init__(self, path=None, resource=None):
        super().__init__(path=MediaExtractor.default_path if not path  else path,
                         resource=MediaExtractor.default_resource if not resource  else resource)

    def read(self, key=None, options=None):
        objects = []
        data = super().read(key=key, options=options)
        if data:
            for object in data:
                objects.append(EObject(object['object']))
        return objects

    def objects(self, status, refresh=False):
        data = None
        if status['id_str']:
            data = self.read(status['id_str'], options={'json': True})
        if not data or refresh:
            try:
                data = []
                if 'entities' in status:
                    if 'media' in status['entities']:
                        for media in status['entities']['media']:
                            data.append(EObject(media['media_url']))

                if status['id_str']:
                    self.write(data, status['id_str'], options={'json': True})
            except:
                pass
        return data


class EntityExtractor(Resource, IExtractor):
    default_path = 'cache'
    default_resource = 'objects/entities'

    def __init__(self, annotator=None, path=None, resource=None):
        super().__init__(path=EntityExtractor.default_path if not path  else path,
                         resource=EntityExtractor.default_resource if not resource  else resource)
        self.annotator = None if not annotator  else annotator

    def read(self, key=None, options=None):
        objects = []
        data = super().read(key=key, options=options)
        if data:
            for object in data:
                objects.append(EObject(object['object']))
        return objects

    def objects(self, status, refresh=False):
        data = None
        if status['id_str']:
            data = self.read(status['id_str'], options={'json': True})
        if not data or refresh:
            try:
                data = []
                entities = self.annotator.entities(status['text'], key=status['id_str'], refresh=refresh)
                for entity in entities:
                    value = entity.label(refresh=refresh)
                    data.append(EObject(value))
                if status['id_str']:
                    self.write(data, status['id_str'], options={'json': True})
            except:
                pass
        return data


class PairExtractor(Resource, IExtractor):
    default_path = 'cache'
    default_resource = 'pair'

    def __init__(self, annotator=None, entyper=None, path=None, resource=None):
        super().__init__(path=PairExtractor.default_path if not path  else path,
                         resource=PairExtractor.default_resource if not resource  else resource)
        self.annotator = None if not annotator  else annotator
        self.entyper = None if not entyper  else entyper

    def read(self, key=None, options=None):
        pairs = []
        data = super().read(key=key, options=options)
        if data:
            for pair in data:
                pairs.append(Pair(pair['attribute'], pair['value']))
        return pairs

    def objects(self, text, key=None, refresh=False):
        return self.pairs(text, key=key, refresh=refresh)

    def pairs(self, text, key=None, refresh=False):
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not data or refresh:
            try:
                data = []
                entities = self.annotator.entities(text, key=key, refresh=refresh)
                for entity in entities:
                    attribute = 'Ø'
                    type_uri = self.entyper.type(entity.uri)
                    if type_uri:
                        type = NamedEntity(type_uri)
                        type.uri = type_uri
                        attribute = type.label(refresh=refresh)
                    value = entity.label(refresh=refresh)
                    data.append(Pair(attribute, value))
                if key:
                    self.write(data, key, options={'json': True})
            except:
                pass
        return data


class LegacyPairExtractor(Resource):
    default_path = 'cache'
    default_resource = 'pairs'
    key = None

    def __init__(self, path=default_path, resource=default_resource, options={}):
        super().__init__(path, resource)
        if 'key' in options:
            self.key = options['key']

    def pos(self, text, status_id):
        pairs = []
        twitterNLP = TwitterNLP()
        res = twitterNLP.pos_tagger(text, status_id)
        before = None
        for word in res['words']:
            if word['tag']:
                if word['tag'].startswith('I-'):
                    pairs[len(pairs) - 1][1] += " " + word['word']
                else:
                    pairs.append([word['tag'], word['word']])
            if word['events']:
                linked = False
                if before:
                    if before['events']:
                        if word['events'] in ['B-EVENT', 'I-EVENT'] and before['events'] in ['B-EVENT', 'I-EVENT']:
                            if before['chunk'] and word['chunk']:
                                if word['chunk'].replace('B-', 'X-').replace('I-', 'X-') == before['chunk'].replace(
                                        'B-', 'X-').replace('I-', 'X-'):
                                    pairs[len(pairs) - 1][1] += " " + word['word']
                                    linked = True
                if not linked:
                    pairs.append([word['events'], word['word']])
            before = word
        return pairs

    def hyponym(self, text, status_id):
        pairs = []
        patterns = [[".*?(\w+)\s+is\s+an?\s+(\w+)", 2, 1],
                    [".*?\s+such\s+(\w+)\s+as\s+(\w+)", 1, 2],
                    [".*?(\w+)\s+or\s+other\s+(\w+)", 2, 1],
                    [".*?(\w+)\s+and\s+other\s+(\w+)", 2, 1],
                    [".*?(\w+)\s+including\s+(\w+)", 1, 2],
                    [".*?(\w+)\s+,?\s+especially\s+(\w+)", 1, 2]]
        for pattern in patterns:
            m = re.match(pattern[0], text.lower())
            if m:
                pairs.append([m.group(pattern[1]), m.group(pattern[2])])
        return pairs

    def dommage(self, text, status_id):
        pairs = []
        patterns = [[".*?(\d+)\s+(dead)", 2, 1],
                    [".*?(\d+)\s+(rescued)", 2, 1],
                    [".*?(\d+)\s+(missed)", 2, 1],
                    [".*?(\d+)\s+(injured)", 2, 1], ]
        for pattern in patterns:
            m = re.match(pattern[0], text.lower())
            if m:
                pairs.append([m.group(pattern[1]), m.group(pattern[2])])
        return pairs

    def dbtypes(self, text, status_id):
        summary = []
        spotlight = Spotlight()
        dbpedia = Dbpedia()
        spots = spotlight.annotate(text, key=status_id, confidence=0.5, support=20)
        uris = []
        if spots:
            if spots.get("Resources"):
                for resource in spots.get("Resources"):
                    uri = resource['@URI']
                    if uri not in uris:
                        attribute = 'Ø'
                        if '@types' in resource:
                            types = re.split(",", resource['@types'])
                            if len(types) > 1:
                                attribute = types[len(types) - 1]
                                fields = re.split(":", attribute)
                                if fields[0] == "DBpedia":
                                    attribute = "http://dbpedia.org/ontology/" + fields[1]
                                attribute = dbpedia.label(attribute)
                        value = uri
                        value = dbpedia.label(value)
                        summary.append([attribute, value])
                        uris.append(uri)
        return summary

    def freq(self, text, status_id):
        summary = []
        spotlight = Spotlight()
        dbpedia = Dbpedia()
        spots = spotlight.annotate(text, key=status_id, confidence=0.25, support=0)
        uris = []
        for resource in spots.get("Resources"):
            uri = resource['@URI']
            if uri not in uris:
                attribute = 'Ø'
                types = {}
                for type in dbpedia.types(uri):
                    if type not in types:
                        # print(type)
                        count = dbpedia.instances(type)
                        types[type] = count
                for type in sorted(types, key=types.get, reverse=True):
                    attribute = type
                    break

                value = uri
                summary.append([dbpedia.label(attribute), dbpedia.label(value)])
                uris.append(uri)
        return summary

    def wikilink(self, text, status_id, sameas=True):
        summary = []
        spotlight = Spotlight()
        dbpedia = Dbpedia()
        spots = spotlight.annotate(text, key=status_id, confidence=0.25, support=0)
        uris = []
        for resource in spots.get("Resources"):
            uri = resource['@URI']
            if uri not in uris:
                attribute = 'Ø'
                types = {}
                similars = [uri]
                if sameas:
                    for same in dbpedia.sameas(uri):
                        similars.append(same)
                for similar in similars:
                    for type in dbpedia.types(similar):
                        if type not in types:
                            types[type] = 0
                        types[type] += 1
                    links = dbpedia.wikipagewikilink(similar)
                    for link in links:
                        for type in dbpedia.types(link):
                            if type not in types:
                                types[type] = 0
                            types[type] += 1
                for type in sorted(types, key=types.get, reverse=True):
                    attribute = type
                    break

                value = uri
                summary.append([dbpedia.label(attribute), dbpedia.label(value)])
                uris.append(uri)
        return summary

    def related(self, text, status_id, sameas=True):

        excluded_uris = ['http://dbpedia.org/property/type', 'http://dbpedia.org/ontology/wikiPageRedirects',
                         'http://dbpedia.org/ontology/type', 'http://www.w3.org/2000/01/rdf-schema#seeAlso']
        excluded_paths = ['wikipedia.org/wiki', 'ontology/wikiPageDisambiguates', 'wikidata.org']
        summary = []
        spotlight = Spotlight()
        dbpedia = Dbpedia()
        spots = spotlight.annotate(text, key=status_id, confidence=0.25, support=0)
        uris = []
        for resource in spots.get("Resources"):
            uri = resource['@URI']
            if uri not in uris:
                # print(uri)
                attribute = 'Ø'
                data = dbpedia.related(uri)
                type_rows = {}
                type_instances = {}
                if data:
                    for binding in data['results']['bindings']:
                        if binding['p']:
                            if binding['p']['type'] and binding['p']['value']:
                                type = binding['p']['type']
                                value = binding['p']['value']
                                rows = binding['rows']['value']

                                if type == 'uri' and value != uri:
                                    exclude = False
                                    if value in excluded_uris:
                                        exclude = True
                                    for path in excluded_paths:
                                        if path in value:
                                            exclude = True
                                    if not exclude:
                                        instances = dbpedia.instances(value)
                                        if value not in type_rows:
                                            type_rows[value] = 0
                                        if value not in type_instances:
                                            type_instances[value] = 0

                                        type_rows[value] += int(rows) * math.log(1 + int(instances))
                                        type_instances[value] += int(instances)
                                        # print('\t' + value + '\t' + str(rows) + '\t' + str(instances) + '')

                rows = sorted(type_rows, key=type_rows.get, reverse=True)
                instances = sorted(type_instances, key=type_instances.get, reverse=True)
                if len(rows) > 0:
                    attribute = rows[0]

                value = uri
                summary.append([dbpedia.label(attribute), dbpedia.label(value)])
                uris.append(uri)
        return summary
