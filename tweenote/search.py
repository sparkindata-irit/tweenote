import json

import requests

from tweenote.cache import Resource
from tweenote.text import TextUtils


class Google(Resource):
    default_path = 'cache'
    default_resource = 'google'
    key = None

    def __init__(self, key=None, path=None, resource=None):
        super().__init__(path=Google.default_path if not path  else path,
                         resource=Google.default_resource if not resource  else resource)
        self.key = Google.key if not key  else key


class Place(Google):
    default_path = 'cache'
    default_resource = 'google/place/search'

    def __init__(self, key=None, path=None, resource=None):
        super().__init__(path=Place.default_path if not path  else path,
                         resource=Place.default_resource if not resource  else resource)
        self.key = Place.key if not key  else key

    def search(self, query):
        key = TextUtils.normalize(query)
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not data:
            uri = 'https://maps.googleapis.com/maps/api/place/textsearch/json'
            params = {'query': query, 'key': self.key}
            headers = {'Accept': 'application/json'}
            response = requests.get(uri, params=params, headers=headers)
            data = json.loads(response.text)
            if response.status_code == 200 and key:
                self.write(data, key, options={'json': True})
        return data
