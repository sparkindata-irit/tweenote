import json


class Pair:
    def __init__(self, attribute, value):
        self.attribute = attribute
        self.value = value

    def __str__(self):
        return '(' + str(self.attribute) + ',' + str(self.value) + ')'

    def json(self):
        return json.dumps(self, default=lambda o: o.__dict__)
