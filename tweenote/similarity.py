import math
from abc import ABCMeta, abstractmethod

from tweenote.annotation import Spotlight


class ISimlarity(metaclass=ABCMeta):
    @abstractmethod
    def compute(self, object1=None, object2=None):
        pass


class CosineSimilarity(ISimlarity):
    @staticmethod
    def __square_rooted(x):
        return math.sqrt(sum([a * a for a in x]))

    def compute(self, x=None, y=None):
        numerator = sum(a * b for a, b in zip(x, y))
        denominator = CosineSimilarity.__square_rooted(x) * CosineSimilarity.__square_rooted(y)
        if denominator == 0:
            return 0
        return numerator / float(denominator)


class EntitySimlarity(CosineSimilarity):
    annotator = Spotlight()

    def __init__(self, annotator=None):
        self.annotator = EntitySimlarity.annotator if not annotator else annotator

    def compute(self, x=None, y=None):
        entites = set()
        xentites = set()
        yentites = set()
        xlist = []
        ylist = []
        for entity in self.annotator.entities(x['text'], x['id_str']):
            entites.add(entity.uri)
            xentites.add(entity.uri)
        for entity in self.annotator.entities(y['text'], y['id_str']):
            entites.add(entity.uri)
            yentites.add(entity.uri)
        for entity in entites:
            if entity in xentites:
                xlist.append(1)
            else:
                xlist.append(0)
            if entity in yentites:
                ylist.append(1)
            else:
                ylist.append(0)
        return super().compute(xlist, ylist)
