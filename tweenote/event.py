import datetime
import json
import math
from abc import ABCMeta, abstractmethod

from sortedcontainers import SortedSet, SortedDict

from tweenote.annotation import Spotlight
from tweenote.extraction import PairExtractor
from tweenote.similarity import EntitySimlarity
from tweenote.social import TweetMaxPopularity
from tweenote.text import TextUtils
from tweenote.typing import TRank


class AbstractStatuses(metaclass=ABCMeta):
    def __init__(self):
        self.__statuses = SortedDict()

    def add(self, status):
        if status:
            if 'id' in status:
                if status['id'] not in self.__statuses:
                    self.__statuses[status['id']] = status
                    return True
        return False

    def add_all(self, statuses):
        count = self.count()
        for status in statuses:
            self.add(status)
        if self.count() > count:
            return True
        return False

    def statuses(self, start=None, end=None):
        start = 0 if not start else start
        end = len(self.__statuses.values()) if not end else end
        return list(self.__statuses.values())[start:end]

    def count(self, start=None, end=None):
        start = 0 if not start else start
        end = len(self.__statuses.values()) if not end else end
        return len(self.statuses(start=start, end=end))

    def status_at(self, index, start=None, end=None):
        return self.statuses(start=start, end=end)[index]


class AbstractSocial(metaclass=ABCMeta):
    @abstractmethod
    def statuses(self, start=None, end=None):
        pass

    @abstractmethod
    def count(self, start=None, end=None):
        pass

    def retweets(self, start=None, end=None):
        retweets = SortedSet()
        # noinspection PyTypeChecker
        for status in self.statuses(start=start, end=end):
            if 'retweeted_status' in status:
                retweets.add(status['id'])
        return retweets

    def retweet_count(self, start=None, end=None):
        return len(self.retweets(start=start, end=end))

    def users(self, start=None, end=None):
        users = SortedSet()
        # noinspection PyTypeChecker
        for status in self.statuses(start=start, end=end):
            if status['user']['id']:
                users.add(status['user']['id'])
        return users

    def user_count(self, start=None, end=None):
        return len(self.users(start=start, end=end))

    def authors(self, start=None, end=None):
        authors = SortedSet()
        # noinspection PyTypeChecker
        for status in self.statuses(start=start, end=end):
            if 'retweeted_status' in status:
                if status['retweeted_status']['user']['id']:
                    authors.add(status['retweeted_status']['user']['id'])
            else:
                if status['user']['id']:
                    authors.add(status['user']['id'])
        return authors

    def author_count(self, start=None, end=None):
        return len(self.authors(start=start, end=end))

    def user_ratio(self, start=None, end=None):
        # noinspection PyTypeChecker
        return self.user_count(start=start, end=end) / self.count(start=start, end=end)

    def author_ratio(self, start=None, end=None):
        # noinspection PyTypeChecker
        return self.author_count(start=start, end=end) / self.count(start=start, end=end)

    def retweet_ratio(self, start=None, end=None):
        # noinspection PyTypeChecker
        return self.retweet_count(start=start, end=end) / self.count(start=start, end=end)

    def interestedness(self, start=None, end=None):
        return self.user_ratio(start=start, end=end) * (self.retweet_ratio(start=start, end=end) + 1)


class AbstractTimeSeries(metaclass=ABCMeta):
    @abstractmethod
    def statuses(self, start=None, end=None):
        pass

    @abstractmethod
    def count(self, start=None, end=None):
        pass

    @abstractmethod
    def status_at(self, index, start=None, end=None):
        pass

    def duration(self, timeunit=datetime.timedelta(days=1), start=None, end=None):
        start = 0 if not start else start
        end = self.count() if not end else end
        duration = TextUtils.datetime(self.status_at(end - 1)['created_at']) - TextUtils.datetime(
            self.status_at(start)['created_at'])
        return math.floor(duration / timeunit) + 1

    def rate(self, timeunit=datetime.timedelta(days=1), start=None, end=None):
        # noinspection PyTypeChecker
        return self.count(start=start, end=end) / self.duration(timeunit, start=start, end=end)

    def ratio(self, reference=None, start=None, end=None):
        if reference:
            return self.count(start=start, end=end) / reference
        return 1.0

    def intensiveness(self, timeunit=datetime.timedelta(days=1), start=None, end=None):
        return self.ratio(start=start, end=end) * self.rate(start=start, end=end, timeunit=timeunit)


class Sequence(AbstractStatuses, AbstractSocial, AbstractTimeSeries):
    def __init__(self):
        super().__init__()
        self.extractor = PairExtractor(annotator=Spotlight(), entyper=TRank())

    def __objects(self, start=None, end=None, extractor=None, refresh=False):
        statuses = self.statuses(start=start, end=end)
        objects = []
        for status in statuses:
            for object in extractor.objects(status, refresh=refresh):
                objects.append(object)
        return objects

    def __pairs_legacy(self, start=None, end=None, refresh=False):
        statuses = self.statuses(start=start, end=end)
        pairs = []
        for status in statuses:
            for pair in self.extractor.pairs(status['text'], key=status['id_str'], refresh=refresh):
                pairs.append(pair)
        return pairs

    def objects(self, start=None, end=None, extractor=None, refresh=False):
        return self.__objects(start=start, end=end, extractor=extractor, refresh=refresh)

    def attributes_values_legacy(self, start=None, end=None, refresh=False):
        attributes = set()
        values = set()
        pairs = []
        for pair in self.__pairs_legacy(start=start, end=end, refresh=refresh):
            attributes.add(pair.attribute)
            values.add(pair.value)
            pairs.append(pair)
        return list(attributes), list(values), pairs

    def cohesiveness(self, simlarity=None, start=None, end=None):
        simlarity = EntitySimlarity() if not simlarity else simlarity
        statuses = self.statuses(start=start, end=end)
        sum_simlarities = 1
        for x in statuses:
            for y in statuses:
                x_ids = {x['id']}
                y_ids = {y['id']}
                if 'retweeted_status' in x:
                    x_ids.add(x['retweeted_status']['id'])
                if 'retweeted_status' in y:
                    y_ids.add(y['retweeted_status']['id'])
                if len(x_ids.intersection(y_ids)) > 0:
                    sum_simlarities += 1
                else:
                    if 'retweeted_status' in x or 'retweeted_status' in x:
                        sum_simlarities += simlarity.compute(x, y)

        return (1 / math.pow(len(statuses), 2)) * sum_simlarities

    def burstiness(self, timeunit=datetime.timedelta(days=1), simlarity=None, intensiveness=True, cohesiveness=False,
                   interestedness=True, start=None, end=None):
        score = 1
        if intensiveness:
            score *= self.intensiveness(start=start, end=end, timeunit=timeunit)
        if cohesiveness:
            score *= self.cohesiveness(start=start, end=end, simlarity=simlarity)
        if interestedness:
            score *= self.interestedness(start=start, end=end)

        return score


class Segment(Sequence):
    def __init__(self):
        super().__init__()
        self.social = TweetMaxPopularity()

    def __sub(self, start=None, end=None):
        segment = Segment()
        sts = self.statuses(start=start, end=end)
        segment.add_all(sts)
        return segment

    def __eligible_k(self, k, start=None, end=None, timeunit=datetime.timedelta(days=1)):
        if start + 1 < k < end - 1:
            d1 = math.floor(TextUtils.datetime(self.status_at(k)['created_at']).timestamp() / timeunit.total_seconds())
            d2 = math.floor(
                TextUtils.datetime(self.status_at(k + 1)['created_at']).timestamp() / timeunit.total_seconds())
            if d2 > d1:
                return True
        return False

    def optimal_k(self, start=None, end=None):
        start = 0 if not start else start
        end = self.count() if not end else end
        best_burstiness = -1
        best_burstiness_x = None
        best_burstiness_y = None
        best_k = -1
        for k in range(start, end - 1):
            if self.__eligible_k(k, start=start, end=end):
                burstiness_x = self.burstiness(start=start, end=k + 1)
                burstiness_y = self.burstiness(start=k + 1, end=end)
                max_burstiness = max(burstiness_x, burstiness_y)
                if max_burstiness > best_burstiness:
                    best_k = k
                    best_burstiness = max_burstiness
                    best_burstiness_x = burstiness_x
                    best_burstiness_y = burstiness_y
        return best_k, best_burstiness_x, best_burstiness_y

    def segments(self, start=None, end=None):
        start = 0 if not start else start
        end = self.count() if not end else end
        (best_k, burstiness_x, burstiness_y) = self.optimal_k(start=start, end=end)
        if best_k != -1:
            # print("BEst K "+str(best_k))
            # print("Day "+self.status_at(best_k)['created_at'])
            return (
                self.__sub(start=start, end=best_k + 1), self.__sub(start=best_k + 1, end=end),
                burstiness_x,
                burstiness_y)
        return None, None, None, None

    def summary(self, start=None, end=None, objects=None, extractor=None, refresh=False):
        (x, y, burstiness_x, burstiness_y) = self.segments(start=start, end=end)
        if not objects:
            objects = self.objects(start=start, end=end, extractor=extractor, refresh=refresh)
        if x and y:
            s_x = x.summary(objects=objects, extractor=extractor, refresh=refresh)
            s_x.weight = burstiness_x
            s_y = y.summary(objects=objects, extractor=extractor, refresh=refresh)
            s_y.weight = burstiness_y
            return s_x.fusion(s_y)
        else:
            return self.intermediate(start=start, end=end, objects=objects, extractor=extractor, refresh=refresh)

    def summary_legacy(self, start=None, end=None, attributes=None, values=None, refresh=False):
        (x, y, burstiness_x, burstiness_y) = self.segments(start=start, end=end)
        if not attributes or not values:
            (attributes, values, pairs) = self.attributes_values_legacy(start=start, end=end, refresh=refresh)
        if x and y:
            s_x = x.summary(attributes=attributes, values=values, refresh=refresh)
            s_x.weight = burstiness_x
            s_y = y.summary(attributes=attributes, values=values, refresh=refresh)
            s_y.weight = burstiness_y
            return s_x.fusion(s_y)
        else:
            return self.intermediate(start=start, end=end, headers=attributes, values=values, refresh=refresh)

    def intermediate(self, start=None, end=None, objects=None, extractor=None, refresh=False):
        statuses = self.statuses(start=start, end=end)
        # noinspection PyUnusedLocal
        vector = [0 for i in range(len(objects))]
        sequence_length = len(statuses)
        for status in statuses:
            social_score = self.social.score(status)
            status_objects = extractor.objects(status, refresh=refresh)
            l = len(status_objects)
            for object in status_objects:
                if object in objects:
                    i = objects.index(object)
                    vi = (1 / l) * social_score * (1 / sequence_length)
                    vector[i] += vi
        return Summary(vector=vector, objects=objects)

    def intermediate_legacy(self, start=None, end=None, attributes=None, values=None, refresh=False):
        statuses = self.statuses(start=start, end=end)
        # noinspection PyUnusedLocal
        matrix = [[0 for i in range(len(values))] for j in range(len(attributes))]
        sequence_length = len(statuses)
        for status in statuses:
            pairs = self.extractor.pairs(status['text'], key=status['id_str'], refresh=refresh)
            social_score = self.social.score(status)
            l = len(pairs)
            for pair in pairs:
                if pair.attribute in attributes and pair.value in values:
                    i = attributes.index(pair.attribute)
                    j = values.index(pair.value)
                    mij = (1 / l) * social_score * (1 / sequence_length)
                    matrix[i][j] += mij
        return Summary_legacy(matrix=matrix, attributes=attributes, values=values)


class Event(Segment):
    def __init__(self):
        super().__init__()

    def load(self, statuses_log):
        ids = set()
        with open(statuses_log, "r", encoding='utf-8') as ins:
            for line in ins:
                # noinspection PyBroadException
                try:
                    status = json.loads(line)
                    if status:
                        if status['id'] not in ids and status['lang'] == 'en':
                            self.add(status)
                            ids.add(status['id'])
                except:
                    pass


class Summary_legacy:
    gamma = 0.75

    def __init__(self, matrix=None, attributes=None, values=None, weight=1, gamma=None):
        self.matrix = matrix
        self.attributes = attributes
        self.values = values
        self.weight = weight
        self.gamma = Summary.gamma if not gamma else gamma

    def fusion(self, s):
        kappa = self.kappa(s)
        # noinspection PyUnusedLocal
        matrix = [[0 for i in range(0, len(self.values))] for j in range(0, len(self.attributes))]
        for i in range(0, len(self.attributes)):
            for j in range(0, len(self.values)):
                matrix[i][j] = (kappa[i][j] / (self.weight + s.weight)) * (
                    self.weight * self.matrix[i][j] + s.weight * s.matrix[i][j])
        return Summary(matrix=matrix, attributes=self.attributes, values=self.values)

    def kappa(self, s):
        sigma = self.sigma(s)
        # noinspection PyUnusedLocal
        matrix = [[0 for i in range(0, len(self.values))] for j in range(0, len(self.attributes))]
        for i in range(0, len(self.attributes)):
            for j in range(0, len(self.values)):
                matrix[i][j] = (self.gamma * math.fabs(self.matrix[i][j] - s.matrix[i][j])) + ((1 - self.gamma) * sigma)
        return matrix

    def sigma(self, s):
        return 1
        #return numpy.matrix(self.deviation_matrix(s)).mean()

    def deviation_matrix(self, s):
        # noinspection PyUnusedLocal
        matrix = [[0 for i in range(0, len(self.values))] for j in range(0, len(self.attributes))]
        for i in range(0, len(self.attributes)):
            for j in range(0, len(self.values)):
                matrix[i][j] = math.fabs(self.matrix[i][j] - s.matrix[i][j])
        return matrix

    def list(self):
        summary = []
        for i in range(0, len(self.attributes)):
            for j in range(0, len(self.values)):
                if self.matrix[i][j] > 0:
                    attribute = self.attributes[i]
                    value = self.values[j]
                    summary.append((attribute, value, self.matrix[i][j]))
        return summary

    def display(self, n=None):
        summary = self.list()
        i = 0
        for pair in sorted(summary, key=lambda x: x[2], reverse=True):
            print('#' + str(i + 1) + '\t(' + pair[0] + ',' + pair[1] + ')\t' + str(pair[2]))
            i += 1
            if n:
                if i >= n:
                    break


class Summary:
    gamma = 0.75

    def __init__(self, vector=None, objects=None, weight=1, gamma=None):
        self.vector = vector
        self.objects = objects
        self.weight = weight
        self.gamma = Summary.gamma if not gamma else gamma

    def fusion(self, s):
        kappa = self.kappa(s)
        # noinspection PyUnusedLocal
        vector = [0 for i in range(0, len(self.objects))]
        for i in range(0, len(self.objects)):
            vector[i] = (kappa[i] / (self.weight + s.weight)) * (
                self.weight * self.vector[i] + s.weight * s.vector[i])
        return Summary(vector=vector, objects=self.objects)

    def kappa(self, s):
        sigma = self.sigma(s)
        # noinspection PyUnusedLocal
        vector = [0 for i in range(0, len(self.objects))]
        for i in range(0, len(self.objects)):
            vector[i] = (self.gamma * math.fabs(self.vector[i] - s.vector[i])) + ((1 - self.gamma) * sigma)
        return vector

    def sigma(self, s):
        if len(s.vector)>0:
            tt=self.deviation_vector(s)
            s=0
            for t in tt:
                s+=t
            return s/len(tt)
        return 0

    def deviation_vector(self, s):
        # noinspection PyUnusedLocal
        vector = [0 for i in range(0, len(self.objects))]
        for i in range(0, len(self.objects)):
            vector[i] = math.fabs(self.vector[i] - s.vector[i])
        return vector

    def list(self):
        summary = []
        for i in range(0, len(self.objects)):
            if self.vector[i] > 0:
                object = self.objects[i]
                summary.append((object, self.vector[i]))
        return summary

    def display(self, n=None):
        summary = self.list()
        i = 0
        for object in sorted(summary, key=lambda x: x[1], reverse=True):
            print('#' + str(i + 1) + "\t" + str(object[0]) + "\t" + str(object[1]))
            i += 1
            if n:
                if i >= n:
                    break
