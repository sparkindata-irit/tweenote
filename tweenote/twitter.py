import datetime
import os
import time

import tweepy

from tweenote.cache import Resource
from tweenote.text import TextUtils


class TwitterAPI(Resource):
    default_path = 'cache'
    default_resource = 'twitter'
    keys = []

    ki = 0

    def __init__(self, path=default_path, resource=default_resource, options={}):
        super().__init__(path, resource)
        if 'consumer_key' in options:
            self.consumer_key = options['consumer_key']
        if 'consumer_secret' in options:
            self.consumer_secret = options['consumer_secret']
        if 'access_token' in options:
            self.access_token = options['access_token']
        if 'access_token_secret' in options:
            self.access_token_secret = options['access_token_secret']
        self.api = None

    def get_api(self):
        if not self.api:
            auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
            auth.secure = True
            auth.set_access_token(self.access_token, self.access_token_secret)
            self.api = tweepy.API(auth, parser=tweepy.parsers.JSONParser())
        return self.api

    def nextApi(self):
        self.ki += 1
        if self.ki >= len(self.keys):
            self.ki = 0
        print("switching key\t" + self.keys[self.ki]['consumer_key'])
        auth = tweepy.OAuthHandler(self.keys[self.ki]['consumer_key'], self.keys[self.ki]['consumer_secret'])
        auth.secure = True
        auth.set_access_token(self.keys[self.ki]['access_token'], self.keys[self.ki]['access_token_secret'])
        self.api = tweepy.API(auth, parser=tweepy.parsers.JSONParser())
        return self.api

    def limit_rate(self, api):
        remaining = 180
        limit = 180
        try:
            rl = api.rate_limit_status()
            remaining = rl['resources']['search']['/search/tweets']['remaining']
            limit = rl['resources']['search']['/search/tweets']['limit']
        except tweepy.error.RateLimitError:
            remaining = 0
        return (remaining, limit)


class TwitterStatus(TwitterAPI):
    default_path = 'cache'
    default_resource = 'twitter/status'

    def __init__(self, path=default_path, resource=default_resource, options={}):
        super().__init__(path, resource, options)

    def status_roller(self, id):
        key = id[0:min(len(id), 8)] + "/" + id;
        data = None
        if key:
            data = self.read(key, options={'json': True})

        if not data:
            api = self.get_api()
            (remaining, limit) = self.limit_rate(api)
            if remaining == 0:
                for a in range(1, 10):
                    api = super().nextApi()
                    (remaining, limit) = self.limit_rate(api)
                    if remaining > 0:
                        break
            if remaining == 0:
                time.sleep(60 * 3)
            try:
                data = api.get_status(id=id)
                if key:
                    self.write(data, key, options={'json': True})
            except tweepy.error.RateLimitError:
                time.sleep(60 * 3)
            except:
                if key:
                    self.write(None, key, options={'json': True})
        return data

    def status(self, id):
        key = id[0:min(len(id), 8)] + "/" + id;
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not os.path.exists(self.file_name(key, options={'json': True})) and not data:
            api = self.get_api()
            try:
                data = api.get_status(id=id)
                if key:
                    self.write(data, key, options={'json': True})
            except:
                pass
        return data


class TwitterSearch(TwitterAPI):
    default_path = 'cache'
    default_resource = 'twitter/search'

    def __init__(self, path=default_path, resource=default_resource, options={}):
        super().__init__(path, resource, options)

    def search(self, query):
        key = TextUtils.normalize(query)
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not data:
            api = self.get_api()
            data = api.search(q=query)
            if key:
                self.write(data, key, options={'json': True})
        return data


class TwitterSearch(TwitterAPI):
    default_path = 'cache'
    default_resource = 'twitter/search'

    def __init__(self, path=default_path, resource=default_resource, options={}):
        super().__init__(path, resource, options)

    def search(self, query, geocode=None, lang=None, locale=None, result_type='recent', count=100, until=None,
               since_id=None, max_id=None, include_entities=True, callback=None, all=True):

        results = {'statuses': []}
        last_count = 0
        first = True
        remaining = 180
        limit = 180
        try:
            while first or (all and last_count != 0):
                key = query + "-" + result_type + "-" + str(count) + "-" + str(since_id) + "-" + str(max_id)
                key = TextUtils.normalize(key)
                print("Search Call " + key)

                data = None
                if key:
                    data = self.read(key, options={'json': True})
                if not data:
                    api = self.get_api()

                    (remaining, limit) = self.limit_rate(api)
                    if remaining == 0:
                        for a in range(1, 10):
                            api = super().nextApi()
                            (remaining, limit) = self.limit_rate(api)
                            if remaining > 0:
                                break
                    print("llllllllllll RATE LIMIT " + str(remaining) + "/" + str(limit))
                    if remaining == 0:
                        print("||||||||||||||||||||    Sleeping for a while")
                        api.wait_on_rate_limit()

                    try:
                        data = api.search(q=query, geocode=geocode, lang=lang, locale=locale, result_type=result_type,
                                          count=count,
                                          until=until, since_id=since_id, max_id=max_id,
                                          include_entities=include_entities,
                                          callback=callback)
                    except tweepy.error.RateLimitError:
                        print("||||||||||||||||||||    Sleeping for a while")
                        api.wait_on_rate_limit()

                    if key:
                        self.write(data, key, options={'json': True})

                if 'statuses' in data:
                    for status in data['statuses']:
                        results['statuses'].append(status)
                        if (not max_id) or status['id'] < max_id:
                            max_id = status['id'] - 1
                    last_count = len(data['statuses'])
                print("Last count " + str(last_count))
                first = False
        except:
            pass
        return results


class UserTimeline2(TwitterAPI):
    default_path = 'cache'
    default_resource = 'twitter/user_timeline'

    def __init__(self, path=default_path, resource=default_resource, options={}):
        super().__init__(path, resource, options)

    def timeline(self, screen_name, count=200, since_id=None, max_id=None, include_rts=1, api=None, level=0,
                 force=False):
        key = TextUtils.normalize(
            screen_name + "-" + str(count) + "-" + str(since_id) + "-" + str(max_id) + "-" + str(include_rts))
        print(key)
        data = None
        old = False
        if key:
            data = self.read(key, options={'json': True})
        if not os.path.exists(self.file_name(key, options={'json': True})):
            data = []
        else:
            mdatetime = format(
                datetime.datetime.fromtimestamp(os.path.getmtime(self.file_name(key, options={'json': True}))))
            print(mdatetime)
            if mdatetime < "2016-05-08":
                old = True

        download = False
        if (force and old) or data == None:
            if not api:
                api = self.get_api()
            data = []
            (remaining, limit) = self.limit_rate(api)
            if remaining == 0:
                for a in range(1, 10):
                    api = super().nextApi()
                    (remaining, limit) = self.limit_rate(api)
                    if remaining > 0:
                        break
            print("~~~~~~~~~~~~~~~~~~~~~~~   RATE LIMIT " + str(remaining) + "/" + str(limit))
            if remaining == 0:
                print("=========================    Sleeping for a while")
                time.sleep(60 * 15)

            try:
                print("-----------    Downloading " + key)
                data = api.user_timeline(screen_name=screen_name, since_id=since_id, max_id=max_id, count=count)
                if key:
                    self.write(data, key, options={'json': True})

            except tweepy.error.RateLimitError:
                print("=========================    Sleeping for a while")
                time.sleep(60 * 15)
            except:
                self.write(data, key, options={'json': True})
                pass
        return data


class UserTimeline(TwitterAPI):
    default_path = 'cache'
    default_resource = 'twitter/user_timeline'

    def __init__(self, path=default_path, resource=default_resource, options={}):
        super().__init__(path, resource, options)

    def timeline(self, screen_name, count=200, since_id=None, max_id=None, include_rts=1, api=None, level=0,
                 force=False):
        key = TextUtils.normalize(
            screen_name + "-" + str(count) + "-" + str(since_id) + "-" + str(max_id) + "-" + str(include_rts))
        data = None
        if key and False:
            data = self.read(key, options={'json': True})
        if not data:
            api = self.get_api()
            data = api.user_timeline(screen_name=screen_name, since_id=since_id, max_id=max_id, count=count,
                                     include_rts=True)
            if key:
                self.write(data, key, options={'json': True})
        return data
