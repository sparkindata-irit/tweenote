import json
import os
import re

import furl
import requests

from tweenote.cache import Resource
from tweenote.text import TextUtils


class LOD(Resource):
    default_path = 'cache'
    default_resource = 'sparql'
    endpoint = None

    def __init__(self, endpoint=None, path=None, resource=None):
        super().__init__(LOD.default_path if not path else path,
                         LOD.default_resource if not resource else resource)
        self.endpoint = LOD.endpoint if not endpoint else endpoint

    def describe(self, uri):
        url = furl.furl(uri)
        path = self.dir_name() + '/describe/' + url.host
        if not os.path.exists(path):
            os.makedirs(path)
        data = None
        key = 'describe/' + str(url.host) + '/' + TextUtils.normalize(str(url.path))
        query = 'DESCRIBE <' + uri + '> '
        if key:
            data = self.read(key, options={'json': True})
        if not data:
            url = self.endpoint + '/sparql'
            params = {'query': query, 'format': 'application/json'}
            headers = {'Accept': 'application/json'}
            response = requests.get(url, params=params, headers=headers)
            data = json.loads(response.text)
            if key:
                self.write(data, key, options={'json': True})
        return data

    def label(self, uri, lang=['en','eng']):

        value = uri
        try:
            entity = self.describe(uri)
            if entity:

                if uri.lower() in entity and uri not in entity:
                    uri = uri.lower();

                if re.sub("http:\/\/dbpedia", "http: \/\/dbpedia", uri) in entity:
                    uri = re.sub("http:\/\/dbpedia", "http: \/\/dbpedia", uri)

                if uri in entity:
                    if 'http://www.w3.org/2000/01/rdf-schema#label' in entity[uri]:
                        if entity[uri]['http://www.w3.org/2000/01/rdf-schema#label']:
                            if not isinstance(entity[uri]['http://www.w3.org/2000/01/rdf-schema#label'],
                                              (list, tuple)):
                                return entity[uri]['http://www.w3.org/2000/01/rdf-schema#label']
                            else:
                                for label in entity[uri]['http://www.w3.org/2000/01/rdf-schema#label']:
                                    try:
                                        if 'value' in label:
                                            if value == uri:
                                                value = label['value']
                                            if 'lang' in label:
                                                if label['lang'] in lang:
                                                    return label['value']
                                    except:
                                        pass
        except:
            pass
        return value


class Dbpedia(LOD):
    path = 'cache'
    resource = 'dbpedia'
    endpoint = 'http://dbpedia.org/'

    def __init__(self, endpoint=None, path=None, resource=None):
        super().__init__(endpoint=Dbpedia.endpoint if not endpoint else endpoint,
                         path=Dbpedia.path if not path else path,
                         resource=Dbpedia.resource if not resource else resource)

    def wikipagewikilink(self, uri):
        cprefix = 'wikipagewikilink'
        results = []
        purl = furl.furl(uri)
        if purl.host in ['dbpedia.org', 'fr.dbpedia.org', 'de.dbpedia.org']:
            path = self.dir_name() + '/' + cprefix + '/' + purl.host
            if not os.path.exists(path):
                os.makedirs(path)
            data = None
            key = cprefix + '/' + str(purl.host) + '/' + TextUtils.normalize(str(purl.path))
            query = 'PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> \n' \
                    'SELECT ?p \n' \
                    'WHERE {  \n' \
                    '{<' + uri + '> dbpedia-owl:wikiPageWikiLink ?p}  \n' \
                                 ' FILTER(!isLiteral(?p)) \n' \
                                 '}  \n' \
                                 'GROUP BY ?p  \n'
            if key:
                data = self.read(key, options={'json': True})
            if not data:
                url = 'http://dbpedia.org/sparql'
                if 'dbpedia.org' in purl.host:
                    url = 'http://' + purl.host + '/sparql'
                params = {'query': query, 'format': 'application/json'}
                headers = {'Accept': 'application/json'}
                response = requests.get(url, params=params, headers={})
                data = json.loads(response.text)
                if key:
                    try:
                        self.write(data, key, options={'json': True})
                    except:
                        pass

            for binding in data['results']['bindings']:
                if binding['p']:
                    if binding['p']['type'] and binding['p']['value']:
                        results.append(binding['p']['value'])
        return results

    def sameas(self, uri):
        cprefix = 'sameas'
        results = []
        purl = furl.furl(uri)
        if 'dbpedia.org' in purl.host and 'pt.dbpedia.org' not in purl.host:
            path = self.dir_name() + '/' + cprefix + '/' + purl.host
            if not os.path.exists(path):
                os.makedirs(path)
            data = None
            key = cprefix + '/' + str(purl.host) + '/' + TextUtils.normalize(str(purl.path))
            query = 'PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> \n' \
                    'SELECT ?p \n' \
                    'WHERE {  \n' \
                    '{<' + uri + '> owl:sameAs ?p}  \n' \
                                 ' FILTER(!isLiteral(?p)) \n' \
                                 '}  \n' \
                                 'GROUP BY ?p  \n'
            if key:
                data = self.read(key, options={'json': True})
            if not data:
                url = 'http://' + purl.host + '/sparql'
                params = {'query': query, 'format': 'application/json'}
                headers = {'Accept': 'application/json'}
                response = requests.get(url, params=params, headers={})
                data = json.loads(response.text)
                if key:
                    try:
                        self.write(data, key, options={'json': True})
                    except:
                        pass
            for binding in data['results']['bindings']:
                if binding['p']:
                    if binding['p']['type'] and binding['p']['value']:
                        results.append(binding['p']['value'])
        return results

    def types(self, uri):
        cprefix = 'types'
        results = []
        purl = furl.furl(uri)
        if 'dbpedia.org' in purl.host and 'pt.dbpedia.org' not in purl.host:
            path = self.dir_name() + '/' + cprefix + '/' + purl.host
            if not os.path.exists(path):
                os.makedirs(path)
            data = None
            key = cprefix + '/' + str(purl.host) + '/' + TextUtils.normalize(str(purl.path))
            query = 'SELECT ?p \n' \
                    'WHERE {  \n' \
                    '{<' + uri + '> rdf:type ?p}  \n' \
                                 ' FILTER(!isLiteral(?p)) \n' \
                                 '}  \n' \
                                 'GROUP BY ?p  \n'
            if key:
                data = self.read(key, options={'json': True})
            if not data:
                url = 'http://' + purl.host + '/sparql'
                params = {'query': query, 'format': 'application/json'}
                headers = {'Accept': 'application/json'}
                try:
                    response = requests.get(url, params=params, headers={})
                    data = json.loads(response.text)
                    if key:
                        self.write(data, key, options={'json': True})
                except:
                    pass
            if data:
                for binding in data['results']['bindings']:
                    if binding['p']:
                        if binding['p']['type'] and binding['p']['value']:
                            results.append(binding['p']['value'])
        return results

    def describe_old(self, uri):
        if not os.path.exists(self.dir_name() + "/describe"):
            os.makedirs(self.dir_name() + "/describe")

        data = None
        key = uri
        key = key.replace('http://dbpedia.org', '')
        key = 'describe/' + TextUtils.normalize(key)
        if key:
            data = self.read(key, options={'json': True})
        if not data:
            try:
                url = 'http://dbpedia.org/sparql/?query=DESCRIBE <' + uri + '>&format=application/json'
                params = {}
                headers = {'Accept': 'application/json'}
                response = requests.get(url, params=params, headers=headers)
                data = json.loads(response.text)
                if key:
                    self.write(data, key, options={'json': True})
            except:
                pass
        return data

    def related_old(self, uri):
        if not os.path.exists(self.dir_name() + "/related"):
            os.makedirs(self.dir_name() + "/related")
        data = None
        key = uri
        key = key.replace('http://dbpedia.org', '')
        key = 'related/' + TextUtils.normalize(key)
        query = 'SELECT ?p count(*) as ?rows \n' \
                'WHERE {  \n' \
                '{?o ?p <' + uri + '>}  \n' \
                                   'UNION  \n' \
                                   '{?p ?o <' + uri + '>}  \n' \
                                                      'UNION  \n' \
                                                      '{<' + uri + '> ?o ?p}  \n' \
                                                                   '  \n' \
                                                                   ' FILTER(!isLiteral(?p)) \n' \
                                                                   '}  \n' \
                                                                   'GROUP BY ?p  \n' \
                                                                   'ORDER BY DESC (?rows) \n'

        if key:
            data = self.read(key, options={'json': True})
        if not data:
            url = 'http://dbpedia.org/sparql'
            query = 'SELECT ?p count(*) as ?rows \n' \
                    'WHERE {  \n' \
                    '{?o ?p <' + uri + '>}  \n' \
                                       'UNION  \n' \
                                       '{?p ?o <' + uri + '>}  \n' \
                                                          'UNION  \n' \
                                                          '{<' + uri + '> ?o ?p}  \n' \
                                                                       '  \n' \
                                                                       ' FILTER(!isLiteral(?p)) \n' \
                                                                       '}  \n' \
                                                                       'GROUP BY ?p  \n' \
                                                                       'ORDER BY DESC (?rows) \n'
            # print(query)
            params = {'query': query, 'format': 'application/json'}
            headers = {'Accept': 'application/json'}
            response = requests.get(url, params=params, headers={})
            data = json.loads(response.text)
            if key:
                self.write(data, key, options={'json': True})
        return data

    def related(self, uri):
        if not os.path.exists(self.dir_name() + "/related"):
            os.makedirs(self.dir_name() + "/related")
        data = None
        key = uri
        key = key.replace('http://dbpedia.org', '')
        key = 'related/' + TextUtils.normalize(key)
        query = 'SELECT ?p count(*) as ?rows \n' \
                'WHERE {  \n' \
                '{?o ?p <' + uri + '>}  \n' \
                                   'UNION  \n' \
                                   '{?p ?o <' + uri + '>}  \n' \
                                                      'UNION  \n' \
                                                      '{?p <' + uri + '> ?o}  \n' \
                                                                      'UNION  \n' \
                                                                      '{?o <' + uri + '> ?p}  \n' \
                                                                                      'UNION  \n' \
                                                                                      '{<' + uri + '> ?o ?p}  \n' \
                                                                                                   'UNION  \n' \
                                                                                                   '{<' + uri + '> ?p ?o}  \n' \
                                                                                                                '  \n' \
                                                                                                                ' {?p rdfs:subClassOf ?x}' \
                                                                                                                ' FILTER(!isLiteral(?p)) \n' \
                                                                                                                '}  \n' \
                                                                                                                'GROUP BY ?p  \n' \
                                                                                                                'ORDER BY DESC (?rows) \n'

        if key:
            data = self.read(key, options={'json': True})
        if not data:
            url = 'http://dbpedia.org/sparql'
            params = {'query': query, 'format': 'application/json'}
            headers = {'Accept': 'application/json'}
            response = requests.get(url, params=params, headers={})
            data = json.loads(response.text)
            if key:
                self.write(data, key, options={'json': True})
        return data

    def instances(self, uri):
        if not os.path.exists(self.dir_name() + "/instances"):
            os.makedirs(self.dir_name() + "/instances")
        data = None
        key = uri
        key = key.replace('http://dbpedia.org', '')
        key = 'instances/' + TextUtils.normalize(key)

        query = 'SELECT count(*) as ?rows \n' \
                'WHERE {  \n' \
                ' {?p rdf:type <' + uri + '>}' \
                                          ' FILTER(!isLiteral(?p)) \n' \
                                          '}  \n'

        if key:
            data = self.read(key, options={'json': True})
        if not data:
            url = 'http://dbpedia.org/sparql'
            params = {'query': query, 'format': 'application/json'}
            headers = {'Accept': 'application/json'}
            response = requests.get(url, params=params, headers={})
            data = json.loads(response.text)
            if key:
                self.write(data, key, options={'json': True})

        if data:
            c = data['results']['bindings'][0]['rows']['value']
            return int(c)
        return 0

    def lookup(self, query):
        if not os.path.exists(self.dir_name() + "/lookup"):
            os.makedirs(self.dir_name() + "/lookup")

        data = None
        key = query
        key = 'lookup/' + TextUtils.normalize(key)
        if key:
            data = self.read(key, options={'json': True})
        if not data:
            url = 'http://lookup.dbpedia.org/api/search/KeywordSearch'
            params = {'QueryString': query}
            headers = {'Accept': 'application/json'}
            response = requests.get(url, params=params, headers=headers)
            data = json.loads(response.text)
            if key:
                self.write(data, key, options={'json': True})
        return data


class Yago(LOD):
    default_path = 'cache'
    default_resource = 'yago'
    endpoint = 'https://linkeddata1.calcul.u-psud.fr'

    def __init__(self, endpoint=None, path=None, resource=None):
        super().__init__(endpoint=Yago.endpoint if not endpoint else endpoint,
                         path=Yago.default_path if not path else path,
                         resource=Yago.default_resource if not resource else resource)

    def label(self, uri, lang='eng', endpoint=None):
        return super().label(uri, lang=lang, endpoint=endpoint)


class Wikidata(Resource):
    default_path = 'cache'
    default_resource = 'wikidata'
    key = None

    def __init__(self, path=default_path, resource=default_resource, options={}):
        super().__init__(path, resource)

    def search(self, query):
        self.path = 'cache'
        self.resource = 'wikidata/search'

        key = TextUtils.normalize(query)
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not data:
            uri = 'https://www.wikidata.org/w/api.php'
            headers = {'Accept': 'application/json'}
            params = {'action': 'wbsearchentities', 'format': 'json', 'search': query, 'language': 'en'}
            response = requests.get(uri, params=params, headers=headers)
            data = json.loads(response.text)
            if response.status_code == 200 and key:
                self.write(data, key, options={'json': True})
        return data

    def entities(self, id):
        self.path = 'cache'
        self.resource = 'wikidata/entities'
        key = TextUtils.normalize(id)
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not data:
            uri = 'https://www.wikidata.org/w/api.php'
            headers = {'Accept': 'application/json'}
            params = {'action': 'wbgetentities', 'format': 'json', 'ids': id, 'language': 'en'}
            response = requests.get(uri, params=params, headers=headers)
            data = json.loads(response.text)
            if response.status_code == 200 and key:
                self.write(data, key, options={'json': True})

        return data

    def label(self, id, lang='en'):
        data = self.entities(id)
        label = data['entities'][id]['labels'][lang]['value']
        return label

    def classes(self, id):
        data = self.entities(id)
        for theclass in data['entities'][id]['claims']['P31']:
            theclassid = theclass['mainsnak']['datavalue']['value']['id']
            label = self.label(theclassid)
            print(label)
