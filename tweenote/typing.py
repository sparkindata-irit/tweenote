import json
import re
from abc import ABCMeta, abstractmethod

import requests

from tweenote.cache import Resource


class IEntyper(metaclass=ABCMeta):
    @abstractmethod
    def type(self, entity=None, key=None):
        pass

    @abstractmethod
    def types(self, entity=None, key=None):
        pass


class TRank(Resource, IEntyper):
    default_path = 'cache'
    default_resource = 'trank'
    endpoint = 'http://ns3038079.ip-5-135-160.eu:5052/'

    def __init__(self, endpoint=None, path=None, resource=None):
        super().__init__(path=TRank.default_path if not path  else path,
                         resource=TRank.default_resource if not resource  else resource)
        self.endpoint = TRank.endpoint if not endpoint  else endpoint

    def type(self, entity=None):
        types = self.types(entity=entity)
        if len(types) > 0:
            return types[0]
        return None

    def types(self, entity=None):
        key = entity.replace('http://dbpedia.org/resource/', 'dbpedia:')
        key = re.sub("\W+", "_", key).lower()
        data = None
        if key and False:
            data = self.read(key, options={'json': True})
        if not data:
            try:
                params = {'uris': entity}
                headers = {'Accept': 'application/json'}
                response = requests.get(self.endpoint, params=params, headers=headers)
                data = json.loads(response.text)
                if key:
                    self.write(data, key, options={'json': True})
            except:
                pass
        types = []
        if data:
            if entity in data:
                types = data[entity]
        return types
