import math
from abc import ABCMeta, abstractmethod


class IScorer(metaclass=ABCMeta):
    @abstractmethod
    def score(self, user):
        pass


class FollowerRatio(IScorer):
    def score(self, user):
        f_in = user['followers_count']
        f_out = user['friends_count']
        s = math.log(1 + f_in) * f_in / (1 + f_in + f_out)
        return s


class SkewedPopularity(IScorer):
    gamma = 1 / 208

    def score(self, user, gamma=None):
        gamma = SkewedPopularity.gamma if not gamma else gamma
        return 1 - math.pow(math.e, -gamma * user['followers_count'])


class TweetMaxPopularity(IScorer):
    def score(self, status, user_scorer=None):
        scorer = SkewedPopularity() if not user_scorer else user_scorer
        score = scorer.score(status['user'])
        if 'retweeted_status' in status:
            retweet_score = scorer.score(status['retweeted_status']['user'])
            score = max(score, retweet_score)
        return score
