import json
import os


class Resource:
    default_path = 'cache'
    default_resource = 'default'

    def __init__(self, path=None, resource=None):
        self.path = Resource.default_path if not path else path
        self.resource = Resource.default_resource if not resource else resource

    def file_name(self, key=None, options=None):
        if not options:
            options = {}
        file_name = None
        if key:
            file_name = self.path + "/" + self.resource + "/" + key
            if options and options['json']:
                file_name += ".json"
        return file_name

    def dir_name(self):
        dir_name = self.path + "/" + self.resource
        return dir_name

    def read(self, key=None, options=None):
        if not options:
            options = {}
        data = None
        file_name = self.file_name(key, options)
        if key and os.path.isfile(file_name) and os.access(file_name, os.R_OK):
            with open(file_name, 'r', encoding='utf-8') as file:
                if options and options['json']:
                    data = json.load(file)
                else:
                    data = file.read()
        return data

    def write(self, data, key=None, options=None):
        if not options:
            options = {}
        dir_name = self.dir_name()
        file_name = self.file_name(key, options)
        if dir_name and not os.path.exists(dir_name):
            os.makedirs(dir_name)
        if not os.path.exists(os.path.dirname(file_name)):
            os.makedirs(os.path.dirname(file_name))
        if key:
            with open(file_name, 'w', encoding='utf-8') as file:
                if options and options['json']:
                    json.dump(data, file, default=lambda o: o.__dict__)
                else:
                    file.write(data)
                return data
