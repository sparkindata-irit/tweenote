import datetime
import re

import pytz


class TextUtils:
    @staticmethod
    def normalize(text):
        text = re.sub("\\W+", " ", text).strip()
        text = re.sub("\\s+", "-", text).lower()
        return text

    @staticmethod
    def uncamel(text):
        text = re.sub('(.)([A-Z][a-z]+)', r'\1 \2', text)
        return re.sub('([a-z0-9])([A-Z])', r'\1 \2', text)

    @staticmethod
    def datetime(created_at):
        created_at = datetime.datetime.strptime(created_at, '%a %b %d %H:%M:%S +0000 %Y')
        created_at = created_at.replace(tzinfo=pytz.timezone('UTC'))
        return created_at
