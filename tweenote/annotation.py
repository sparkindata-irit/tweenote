import json
from abc import ABCMeta, abstractmethod

import requests

from tweenote.cache import Resource
from tweenote.entities import NamedEntity
from tweenote.lod import Dbpedia
from tweenote.lod import Yago


class IAnnotator(Resource, metaclass=ABCMeta):
    default_path = 'cache'
    default_resource = 'annotator'

    def __init__(self, path=None, resource=None):
        super().__init__(path=IAnnotator.default_path if not path else path,
                         resource=IAnnotator.default_resource if not resource else resource)

    @abstractmethod
    def annotate(self, text, key=None, refresh=False):
        pass

    @abstractmethod
    def entities(self, text, key=None, refresh=False):
        pass


class Aida(IAnnotator):
    default_path = 'cache'
    default_resource = 'aida'
    endpoint = 'https://gate.d5.mpi-inf.mpg.de/aida/service/disambiguate'

    def __init__(self, endpoint=endpoint, path=None, resource=None):
        super().__init__(path=Aida.default_path if not path else path,
                         resource=Aida.default_resource if not resource else resource)
        self.endpoint = Aida.endpoint if not endpoint else endpoint

    def annotate(self, text, key=None, refresh=False):
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not data or refresh:
            try:
                params = {'text': text}
                response = requests.post(self.endpoint, data=params)
                data = json.loads(response.text)
                if key:
                    self.write(data, key, options={'json': True})
            except:
                pass
        return data

    def entities(self, text, key=None, refresh=False):
        entities = []
        data = self.annotate(text, key=key, refresh=refresh)
        yago = Yago()
        if data:
            if 'mentions' in data:
                for mention in data['mentions']:
                    entity = NamedEntity(mention['bestEntity']['kbIdentifier']);
                    entity.uri = 'http://yago-knowledge.org/resource/' + mention['bestEntity']['kbIdentifier'].replace(
                        'YAGO:', '')
                    entity.name = yago.label(entity.uri)
                    entity.mention = text[mention['offset']:mention['offset'] + mention['length']]
                    entity.score = float(mention['bestEntity']['disambiguationScore'])
                    entity.annotator = self.__class__.__name__
                    entities.append(entity)
        return entities


class Babelfy(IAnnotator):
    default_path = 'cache'
    default_resource = 'babelfy'

    endpoint = 'https://babelfy.io/v1/disambiguate'
    lang = 'en'
    key = None

    def __init__(self, endpoint=None, lang=None, key=None, path=None, resource=None):
        super().__init__(path=Babelfy.default_path if not path else path,
                         resource=Babelfy.default_resource if not resource else resource)
        self.endpoint = Babelfy.endpoint if not endpoint else endpoint
        self.lang = Babelfy.lang if not lang else lang
        self.key = Babelfy.key if not key else key

    def annotate(self, text, key=None, refresh=False):
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not data or refresh:
            try:
                params = {'text': text, 'lang': self.lang, 'key': self.key}
                headers = {'Accept': 'application/json'}
                response = requests.get(self.endpoint, params=params, headers=headers)
                data = json.loads(response.text)
                if key:
                    self.write(data, key, options={'json': True})
            except:
                pass
        return data

    def entities(self, text, key=None, refresh=False):
        entities = []
        data = self.annotate(text, key=key, refresh=refresh)
        if data:
            dbpedia = Dbpedia();
            for item in data:
                if 'DBpediaURL' in item:
                    if item['DBpediaURL']:
                        entity = NamedEntity(item['DBpediaURL'].replace('http://dbpedia.org/resource/', 'DBpedia:'))
                        entity.uri = item['DBpediaURL']
                        entity.name = dbpedia.label(item['DBpediaURL'])
                        entity.mention = text[item['charFragment']['start']:item['charFragment']['end'] + 1]
                        entity.score = float(item['score'])
                        entity.annotator = self.__class__.__name__
                        entities.append(entity)

        return entities


class Spotlight(IAnnotator):
    default_path = 'cache'
    default_resource = 'spotlight'

    host = 'spotlight.sztaki.hu'
    port = 2222
    confidence = 0.25
    support = 0

    def __init__(self, host=None, port=None, confidence=None, support=None, path=None, resource=None):
        super().__init__(path=Spotlight.default_path if not path else path,
                         resource=Spotlight.default_resource if not resource else resource)
        self.host = Spotlight.host if not host else host
        self.port = Spotlight.port if not port else port
        self.confidence = Spotlight.confidence if not confidence else confidence
        self.support = Spotlight.support if not support else support

    def get_uri(self):
        return 'http://' + str(self.host) + ':' + str(self.port) + '/rest/annotate/'

    def annotate(self, text, key=None, refresh=False):
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not data or refresh:
            try:
                uri = self.get_uri()
                params = {'text': text, 'confidence': self.confidence, 'support': self.support}
                headers = {'Accept': 'application/json'}
                response = requests.get(uri, params=params, headers=headers)
                data = json.loads(response.text)
                if key:
                    self.write(data, key, options={'json': True})
            except:
                pass
        return data

    def entities(self, text, key=None, refresh=False):
        entities = []
        data = self.annotate(text, key=key, refresh=refresh)
        if data:
            dbpedia = Dbpedia()
            if 'Resources' in data:
                for resource in data['Resources']:
                    if '@URI' in resource:
                        entity = NamedEntity(resource['@URI'].replace('http://dbpedia.org/resource/', 'DBpedia:'));
                        entity.uri = resource['@URI']
                        entity.name = dbpedia.label(resource['@URI'])
                        entity.mention = resource['@surfaceForm']
                        entity.score = resource['@similarityScore']
                        entity.annotator = self.__class__.__name__
                        entities.append(entity)
        return entities


class TagMe(IAnnotator):
    default_path = 'cache'
    default_resource = 'tagme'

    endpoint = 'https://tagme.d4science.org/tagme/tag'
    gcube_token = None
    lang = 'en'
    tweet = False
    include_abstract = False
    include_categories = False
    epsilon = 0.5

    def __init__(self, endpoint=None, gcube_token=None, lang=None,
                 tweet=None, include_abstract=None, include_categories=None, epsilon=None, path=None, resource=None):
        super().__init__(path=TagMe.default_path if not path else path,
                         resource=TagMe.default_resource if not resource else resource)
        self.endpoint = TagMe.endpoint if not endpoint else endpoint
        self.gcube_token = TagMe.gcube_token if not gcube_token else gcube_token
        self.lang = TagMe.lang if not endpoint else lang
        self.tweet = TagMe.tweet if not tweet else tweet
        self.include_abstract = TagMe.include_abstract if not include_abstract else include_abstract
        self.include_categories = TagMe.include_categories if not include_categories else include_categories
        self.epsilon = TagMe.epsilon if not epsilon else epsilon

    def annotate(self, text, key=None, refresh=False):
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not data or refresh:
            try:
                params = {'text': text, 'gcube-token': self.gcube_token, 'lang': self.lang,
                          'tweet': self.tweet, 'include_abstract': self.include_abstract,
                          'include_categories': self.include_categories, 'epsilon': self.epsilon}
                headers = {'Accept': 'application/json'}
                response = requests.get(self.endpoint, params=params, headers=headers)
                data = json.loads(response.text)
                if key:
                    self.write(data, key, options={'json': True})
            except:
                pass
        return data

    def entities(self, text, key=None, refresh=False):
        entities = []
        data = self.annotate(text, key=key, refresh=refresh)
        if data:
            if 'annotations' in data:
                for annotation in data['annotations']:
                    if 'title' in annotation:
                        entity = NamedEntity('DBpedia:' + annotation['title'].replace(' ', '_'));
                        entity.uri = 'http://dbpedia.org/resource/' + annotation['title'].replace(' ', '_')
                        entity.name = annotation['title']
                        entity.mention = annotation['spot']
                        entity.score = annotation['link_probability']
                        entity.annotator = self.__class__.__name__
                        entities.append(entity)
        return entities


class TwitterNLP(IAnnotator):
    default_path = 'cache'
    default_resource = 'twitter-nlp'

    endpoint = 'http://locahost:5051/'

    def __init__(self, endpoint=None, path=None, resource=None):
        super().__init__(path=TwitterNLP.default_path if not path else path,
                         resource=TwitterNLP.default_resource if not resource else resource)
        self.endpoint = TwitterNLP.endpoint if not endpoint else endpoint

    def annotate(self, text, key=None, refresh=False):
        return self.pos_tagger(text, key=key, refresh=refresh)

    def pos_tagger(self, text, key=None, refresh=False):
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not data or refresh:
            try:
                params = {'text': text, 'key': key}
                headers = {'Accept': 'application/json'}
                response = requests.get(self.endpoint, params=params, headers=headers)
                data = json.loads(response.text)
                if key:
                    self.write(data, key, options={'json': True})
            except:
                data = {'text': text, 'words': []}
        return data

    def dbpedia_type(self, tag):
        tyes = {
            'band': 'DBpedia:band',
            'company': 'DBpedia:company',
            'geo-loc': 'DBpedia:Location',
            'facility': 'DBpedia:Facility',
            'movie': 'DBpedia:movie',
            'musicartist': 'DBpedia:MusicalArtist',
            'product': 'DBpedia:product',
            'person': 'DBpedia:Person',
            'sportsteam': 'DBpedia:SportsTeam',
            'tvshow': 'DBpedia:TelevisionShow',
            'EVENT': 'DBpedia:Event'
        }
        if tag in tyes:
            return tyes[tag]
        return tag

    def entities(self, text, key=None, refresh=False):
        entities = []
        dbpedia = Dbpedia()
        data = self.annotate(text, key=key, refresh=refresh)
        words = []
        k = -1
        incremental = False
        if data:
            if 'words' in data:
                for word in data['words']:

                    if word['chunk'] and word['tag']:
                        if word['chunk'].startswith('B') or word['tag'].startswith('B'):
                            words.append({'tag': word['tag'].replace('B-', ''), 'word': word['word']})
                            k += 1
                            incremental = True
                        else:
                            words[k]['word'] += ' ' + word['word']
                    elif word['events']:
                        words.append({'tag': word['events'].replace('B-', ''), 'word': word['word']})
                        k += 1
                        incremental = False
                    else:
                        incremental = False
        for word in words:
            best_match_label = None
            best_match_uri = None
            matchs = dbpedia.lookup(word['word'])
            if matchs:
                matchs = sorted(matchs['results'], key=lambda x: x['refCount'], reverse=True)
                for match in matchs:
                    best_match_label = match['label']
                    best_match_uri = match['uri']
                    break
            entity = NamedEntity('nlp:' + word['word']) if not best_match_uri else  NamedEntity(
                best_match_uri.replace('http://dbpedia.org/resource/', 'DBpedia:'))
            entity.uri = word['word'] if not best_match_uri else  best_match_uri
            entity.name = word['word'] if not best_match_label else  best_match_label
            entity.mention = word['word']
            entity.score = 1
            entity.types = [self.dbpedia_type(word['tag'])]
            entity.annotator = self.__class__.__name__
            entities.append(entity)
        return entities


class Wikifier(IAnnotator):
    default_path = 'cache'
    default_resource = 'wikifier'

    endpoint = 'http://www.wikifier.org/annotate-article'
    lang = 'en'
    key = None

    def __init__(self, endpoint=None, lang=None, key=None, path=None, resource=None):
        super().__init__(path=Wikifier.default_path if not path else path,
                         resource=Wikifier.default_resource if not resource else resource)
        self.endpoint = Wikifier.endpoint if not endpoint else endpoint
        self.lang = Wikifier.lang if not lang else lang
        self.key = Wikifier.key if not key else key

    def annotate(self, text, key=None, refresh=False):
        data = None
        if key:
            data = self.read(key, options={'json': True})
        if not data or refresh:
            try:
                params = {'text': text, 'userKey': self.key, 'lang': self.lang}
                headers = {'Accept': 'application/json'}
                response = requests.get(self.endpoint, params=params, headers=headers)
                data = json.loads(response.text)
                if key:
                    self.write(data, key, options={'json': True})
            except:
                pass
        return data

    def entities(self, text, key=None, refresh=False):
        entities = []
        data = self.annotate(text, key=key, refresh=refresh)
        if data:
            if 'annotations' in data:
                for annotation in data['annotations']:
                    entity = NamedEntity(annotation['dbPediaIri'].replace('http://dbpedia.org/resource/', 'DBpedia:'))
                    entity.uri = annotation['dbPediaIri']
                    entity.name = annotation['title']
                    entity.mention = None
                    page_rank = - 1
                    for support in annotation['support']:
                        if support['pageRank'] >= page_rank:
                            entity.mention = ''
                            for x in range(support['wFrom'], support['wTo'] + 1):
                                if entity.mention:
                                    entity.mention += ' '
                                entity.mention += data['words'][x]
                    entity.score = annotation['cosine']
                    entity.annotator = self.__class__.__name__
                    entities.append(entity)
        return entities
