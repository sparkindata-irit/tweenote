============
Tweenote
============

Tweenote is python library for tweet annotation and event summarization.



Requirement
=================

- Python 3.5



Setup project
=================

Tweenote is open source project available on Bitbucket. To clone the repository, use the following command.

.. code-block:: bash

	git clone https://amjedbj@bitbucket.org/sparkindata-irit/tweenote.git
	cd tweenote
	chmod a+x bin/*


You need to setup configuration parameters in `config.ini <config.ini>`_.




Text annotation
=================

In order to annotate a text with named entities, use the next command. Annotation method may be selected with *--method* parameter from the following list.

- `DBpedia Spotlight <https://dbpedia-spotlight.github.io/demo/>`_ (spotlight)
- `TagMe <https://tagme.d4science.org/tagme/>`_ (tagme)
- `Aida <http://www.mpi-inf.mpg.de/departments/databases-and-information-systems/research/yago-naga/aida/>`_ ( aida)
- `Babelfy <http://babelfy.org/>`_ (babelfy)
- `Wikifier <https://cogcomp.cs.illinois.edu/page/demo_view/Wikifier>`_ (wikifier)
- `TwitterNLP <https://github.com/aritter/twitter_nlp>`_ (twitternlp)

.. code-block:: bash

	./bin/annotation --method spotlight --text "Wikipedia is a free online encyclopedia"


Extract pairs
=================

The following command line enable to extract attribute-value pairs from a text.



.. code-block:: bash

	./bin/pairs --text "Wikipedia is free online encyclopedia and the the largest reference work on the Internet."


Summarize tweets
=================

The following command line  enable to generate a summary of attribute-value pairs from tweet status log file.


.. code-block:: bash

	./bin/summary --s statuses_log data/status.log --out summary.csv



Version
===============

Tweenote 1.0.2

Citation
===============

If you use this code, please cite this paper:

    Lamjed Ben Jabeur, Lynda Tamine, and Yoan Pitarch. Someone please tell me what is happening? Towards intelligible summaries of live events. Technical Report. University of Paul Sabatier, Toulouse, 2006.


Contributors
===============

The following people have contributed to this code:

- Lamjed Ben Jabeur `Lamjed.Ben-Jabeur@irit.fr <mailto:Lamjed.Ben-Jabeur@irit.fr>`_.


License
===============

This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software.  You can  use, modify and/ or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
`http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html <http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>`_.
